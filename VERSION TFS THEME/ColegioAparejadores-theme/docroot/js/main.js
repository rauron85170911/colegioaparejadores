AUI().ready(

	/*
	This function gets loaded when all the HTML, not including the portlets, is
	loaded.
	*/

	function () {
		// JavaScript Document

		jQuery(document).ready(function () {

			function iframeCarga() {
				$('iframe').contents().find("html.aui").css('background', 'white');
			}
			$('.aui').on('click', '.journal-article-wrapper-content', function () {
				if (this.find('a.cke_button_off')) {
					iframeCarga();
				}
			});
			setTimeout(function () {
				iframeCarga();
			}, 5000);
			if ($('html.aui').find('cke')) {
				$('html.aui').css('background', 'white');
				$('html.aui').addClass('blanco');
			}
			/* VARIABLES GLOBALES */

			var $body = $('body');
			var $aui = $('html.aui');

			/* Recoge valores url */
			/*var pathname = window.location.pathname;*/

			// Comprobamos valor de pathname
			// alert(pathname); 

			/* A traves del pathname añadimos la clase css padre en el body para la herenecia de estilos css */
			/*if (pathname === "/web/guest/agenda" || pathname === "/agenda") {
				$body.addClass('agenda');
			} else if (pathname === "/web/guest/home" || pathname === "/home" ||
					   pathname === "/web/guest/home-2" || pathname === "/home-2") {
				$body.addClass('home');
			} else if (pathname === "/web/guest/servicios" || pathname === "/servicios",
					   pathname === "/web/guest/archivo-documental" || pathname === "/archivo-documental",
					   pathname === "/web/guest/archivo-documental/publicaciones" || pathname === "/archivo-documental/publicaciones",
					   pathname === "/web/guest/archivo-documental/documentacion-sobre-las-jornadas" || pathname === "/archivo-documental/documentacion-sobre-las-jornadas",
					   pathname === "/web/guest/archivo-documental/tarifas" || pathname === "/archivo-documental/documentacion-sobre-las-jornadas/tarifas",
					   pathname === "/web/guest/archivo-documental/archivos-y-registros" || pathname === "/archivo-documental/archivos-y-registros",
					   pathname === "/web/guest/servicios/encuentra-un-profesional" || pathname === "/servicios/encuentra-un-profesional",
					   pathname === "/web/guest/servicios/alquiler-de-espacios" || pathname === "/servicios/alquiler-de-espacios",
					   pathname === "/web/guest/servicios/bolsa-de-trabajo" || pathname === "/servicios/bolsa-de-trabajo",
					   pathname === "/web/guest/servicios/colaboraciones" || pathname === "/servicios/colaboraciones",
					   pathname === "/web/guest/gabinete-orientacion-empresarial" || pathname === "/gabinete-orientacion-empresarial",
					   pathname === "/web/guest/gabinete-orientacion-empresarial/talleres" || pathname === "/gabinete-orientacion-empresarial/talleres",
					   pathname === "/web/guest/gabinete-orientacion-empresarial/coaching-individual" || pathname === "/gabinete-orientacion-empresarial/coaching-individual",
					   pathname === "/web/guest/gabinete-orientacion-empresarial/coaching-grupal" || pathname === "/gabinete-orientacion-empresarial/coaching-grupal",
					   pathname === "/web/guest/gabinete-orientacion-empresarial/conferencias" || pathname === "/gabinete-orientacion-empresarial/conferencias",
					   pathname === "/web/guest/gabinete-orientacion-empresarial/mentoling" || pathname === "/gabinete-orientacion-empresarial/mentoling",
					   pathname === "/web/guest/gabinete-prensa" || pathname === "/gabinete-prensa",
					   pathname === "/web/guest/gabinete-prensa" || pathname === "/gabinete-prensa",
					   pathname === "/web/guest/gabinete-prensa/comunicaciones" || pathname === "/gabinete-prensa/comunicaciones",
					   pathname === "/web/guest/gabinete-prensa/banco-imagenes" || pathname === "/gabinete-prensa/banco-imagenes",
					   pathname === "/web/guest/gabinete-prensa/informacion-corporativa" || pathname === "/gabinete-prensa/informacion-corporativa",
					   pathname === "/web/guest/gabinete-prensa/contacto" || pathname === "/gabinete-prensa/contacto",
					   pathname === "/web/guest/zona-privada/servicios-juridicos" || pathname === "/zona-privada/servicios-juridicos",
					   pathname === "/web/guest/zona-privada/servicios-juridicos/desarrollo-empresarial" || pathname === "/zona-privada/servicios-juridicos/desarrollo-empresarial",
					   pathname === "/web/guest/zona-privada/gabinete-tecnico" || pathname === "/zona-privada/servicios-juridicos/gabinete-tecnico",
					   pathname === "/web/guest/zona-privada/gabinete-tecnico/legislacion-y-normativa" || pathname === "/zona-privada/servicios-juridicos/gabinete-tecnico/legislacion-y-normativa",
					   pathname === "/web/guest/zona-privada/gabinete-tecnico/legislacion-y-normativa/instalaciones" || pathname === "/zona-privada/servicios-juridicos/gabinete-tecnico/legislacion-y-normativa/instalaciones",
					   pathname === "/web/guest/zona-privada/tramites-digitales" || pathname === "/zona-privada/tramites-digitales",
					   pathname === "/web/guest/zona-privada/tramites-digitales/biblioteca" || pathname === "/zona-privada/tramites-digitales/biblioteca",
					   pathname === "/web/guest/zona-privada/visados" || pathname === "/zona-privada/visados",
					   pathname === "/web/guest/zona-privada/visados/impresos" || pathname === "/zona-privada/visados/impresos",
					   pathname === "/web/guest/zona-privada/visados/libro-de-ordenes-electronico" || pathname === "/zona-privada/visados/libro-de-ordenes-electronico",
					   pathname === "/web/guest/zona-privada/visados/informacion-sobre-el-visado" || pathname === "/zona-privada/visados/informacion-sobre-el-visado",
					   pathname === "/web/guest/zona-privada/visados/libro-de-incidencia-electronica" || pathname === "/zona-privada/visados/libro-de-incidencia-electronica") {
				$body.addClass('servicios');
			} else if (pathname === "/web/guest/servicios-detalle" || pathname === "/servicios-detalle" ||
					   pathname === "/web/guest/el-colegio" || pathname === "/el-colegio") {
				$body.addClass('servicios');
				$body.addClass('pagina_detalle');
			} else if (pathname === "/web/guest/empresas" || pathname === "/empresas",
					   pathname === "/web/guest/empresa" || pathname === "/empresa",
					   pathname === "/web/guest/empresa/encuentra-un-profesional" || pathname === "/empresa/encuentra-un-profesional",
					   pathname === "/web/guest/el-colegio/empresas-amigas" || pathname === "/el-colegio/empresas-amigas",
					   pathname === "/web/guest/el-colegio/empresas-amigas" || pathname === "/el-colegio/empresas-amigas",
					   pathname === "/web/guest/empresa/visados" || pathname === "/empresa/visados",
					   pathname === "/web/guest/empresa/medios-gestion" || pathname === "/empresa/visados/medios-gestion",
					   pathname === "/web/guest/ciudadano" || pathname === "/ciudadano",
					   pathname === "/web/guest/ciudadano/bolsa-de-trabajo" || pathname === "/ciudadano/bolsa-de-trabajo",
					   pathname === "/web/guest/ciudadano/colaboraciones" || pathname === "/ciudadano/colaboraciones") {
				$body.addClass('empresas');
			} else if (pathname === "/web/guest/formacion" || pathname === "/formacion",
					   pathname === "/web/guest/formacion-y-cultura" || pathname === "/formacion-y-cultura",
					   pathname === "/web/guest/formacion-y-cultura/cursos-online" || pathname === "/formacion-y-cultura/cursos-online",
					   pathname === "/web/guest/formacion-y-cultura/fundacion-escuela-de-la-edificacion" || pathname === "/formacion-y-cultura/fundacion-escuela-de-la-edificacion",
					   pathname === "/web/guest/formacion-y-cultura/actividades-culturales" || pathname === "/formacion-y-cultura/actividades-culturales",
					   pathname === "/web/guest/el-colegio/observatorio-2020" || pathname === "/el-colegio/observatorio-2020",
					   pathname === "/web/guest/gabinete-prensa/notas-de-prensa" || pathname === "/gabinete-prensa/notas-de-prensa") {
				$body.addClass('formacion');
			} else { // si no esta creada la pagina en liferay por defecto se añade la clase servicios
				// $body.addClass('home');
				$body.addClass('servicios');
			} */
			/* Si estamos logados Añadimos la clase 'colegiado' en el body para la herencia de estilos css */
			if (Liferay.ThemeDisplay.isSignedIn()) {
				//alert('Hello ' + Liferay.ThemeDisplay.getUserName() + '. Welcome Back.')
				$body.addClass('colegiado');
			} else {
				//alert('Hello Guest.')
			}

			/* Añadimos estilos al portlet de login de liferay */
			$portletLogin = $('.portlet-login');
			$portlet101 = $('#portlet_101');
			// $contenidoLogin = $portletLogin.closest('.portlet-column');
			$portletLogin.closest('body').addClass('login');
			if ($portlet101.length) {
				$('body').addClass('servicios');
				// console.log('Existe');
			} else {
				// console.log('No existe');
			}
			if ($portletLogin.length) {
				// console.log('Existe');
				$('.CA_contenedor .fondo-aux').addClass('imagen-contraida');
			} else {
				// console.log('No existe')
			}

			/* Añadimos funcionalidad para el portlet de lsitadp de formularios */
			$contenedorResultadosBusquda = $aui.find('table[data-searchcontainerid="_3_documentsSearchContainer"]');
			if ($contenedorResultadosBusquda.length) {
				// console.log('Existe');
				$('body').addClass('buscador');
				$('.portlet-search .portlet-borderless-container').addClass('container-fluid');
				$('.portlet-search .portlet-borderless-container .input-container').attr('id', 'buscador');
				$('.portlet-search .portlet-borderless-container .input-container').prepend('<h1>BUSCADOR</h1>');
				$('.portlet-search .portlet-borderless-container .input-container .asset-entry-tags').prepend('<i class="icon-etiqueta"></i>');
				$('.lfr-search-container table').removeAttr('class');
			} else {
				// console.log('No existe');
			}
			// Añadimos label 'Filtros:' para respetar diseño
			$('.aui body.buscador div.liferaytokenlist-content').prepend('<span class="filtros"><span>Filtros: </span><i class="icon-etiqueta"></i></span>');

			/* Añado placeholder buscador */
			$('#_3_keywords').attr('placeholder', 'Buscar');
			// menu desplegable cabeceras

			// Funcionalidad para web-form-portlet
			var $contenedorFormulario = $aui.find('div.web-form-portlet form fieldset > div');
			$aui.find('div.web-form-portlet form').addClass('container-fluid');
			$aui.find('div.web-form-portlet form fieldset').addClass('formu_desplegable');
			$contenedorFormulario.addClass('box_content collapse in');
			$contenedorFormulario.each(function (i) {
				$(this).attr('id', 'desplegable-' + (i + 1));
				var $valorid = $(this).attr('id');
				$(this).prepend('<a role="button" class="desplegar" data-toggle="collapse" href="#' + $valorid + '" aria-expanded="true" aria-controls="formu_busc_pro"><span>Desplegar</span></a>');
			});
			//console.log($valorid);
			jQuery(function () {
				var b_main = $(".navbar-toggle");
				var b_desp = $(".cab-nav .nav-item ");
				var desplegable = $('#navbar');

				function desplegar(tl) {
					//		console.log('abre: ' + this);
					tl.parent().children('#navbar').stop(true, true).delay(0).slideDown(200);
					tl.toggleClass('open');
				}

				function plegar(tl) {
					//	console.log('cierra: ' + tl);
					tl.parent().children('#navbar').stop(true, true).delay(0).slideUp(200);
					tl.toggleClass('open');
				}

				function desplegarSub(tl) {
					//console.log(this);
					tl.siblings('.nav-item').children('.sub-menu').stop(true, true).delay(0).slideUp(200); //recoge otros dropdowns sin delay
					tl.children('.sub-menu').stop(true, true).delay(100).slideDown(200);
					tl.toggleClass('drop_over');
				}

				function plegarSub(tl) {
					//console.log(tl);
					tl.children('.sub-menu').stop(true, true).delay(500).slideUp(200);
					tl.toggleClass('drop_over');
				}


				var ancho_min = 630;
				var ancho_med = 930;
				var ancho_max = 1280;


				b_desp.hover(function () {
					desplegarSub(jQuery(this));
				}, function () {
					plegarSub(jQuery(this));
				});


				b_main.click(function () {
					desplegar(jQuery(this));
				});

				b_main.click(function () {
					if (desplegable.is(':hidden') == true) {
						desplegar(jQuery(this));
					} else {
						plegar(jQuery(this));
					}
				});


				$(document).on('click', function (event) {
					if (!$(event.target).closest('.navbar').length) {
						// stopping-event-propagation
						// console.log('cierra');
						plegar($('#navbar'));
					}
				});

			});




			/*-----------------------resize servicios home------------------------*/

			// hack para equiparar los anchos del @media query con los de jquery
			$('body').append("<div id='aux'><div></div></div>");

			var wndW;
			var control_w;

			var screen_xxs = 390;
			var screen_xs = 480;
			var screen_s = 570;
			var screen_is = 620;
			var screen_sm = 768;
			var screen_md = 992;
			var screen_lg = 1260;

			function width_trace() {
				wndW = $(window).width();
				control_w = $("body").find('#aux div').width();
				// console.log("window width: " + wndW + " , control_w: " + control_w);
			}

			width_trace();
			calcularAlto();
			calcularAlto2();
			calcularalto3();

			$(window).resize(function () {
				width_trace();
				calcularAlto();
				calcularAlto2();
				calcularalto3();
				//     console.log ("resize");
			});



			$(window).on("orientationchange", function (event) {
				// console.log( "Este dispositivo está en modo " + event.orientation);
				width_trace();
				calcularAlto();
				calcularAlto2();
				calcularalto3();
			});

			// ajuste de alto de los parrafos de mundo servicios
			function calcularAlto() {
				alto_1 = $('.auto_height_1 p').outerHeight();
				alto_2 = $('.auto_height_2 p').outerHeight();
				alto_3 = $('.auto_height_3 p').outerHeight();


				// console.log(alto_1 + " , " + alto_2 + " , " + alto_3 + " ,el más alto: " + mas_alto);
				// console.log("window: " + wndW + " control: " + control_w );


				if (control_w >= screen_sm) {
					mas_alto = Math.max(alto_1, alto_2, alto_3);
					$('.auto_height_1').height(mas_alto);
					$('.auto_height_2').height(mas_alto);
					$('.auto_height_3').height(mas_alto);
				}
				if ((control_w < screen_sm) & (control_w >= screen_xs)) {
					//		console.log("menos");
					mas_alto = Math.max(alto_1, alto_2);
					$('.auto_height_1').height(mas_alto);
					$('.auto_height_2').height(mas_alto);
					$('.auto_height_3').height('auto');
				};
				if (control_w < screen_xs) {
					$('.auto_height_1').height('auto');
					$('.auto_height_2').height('auto');
					$('.auto_height_3').height('auto');
				}

			}


			// ajuste de alto de los bloques menú
			function calcularalto3() {

				var altos = [];
				$('#menu_completo > ul > li').each(function () {
					altos.push($(this).height());
					//   console.log($(this).height());
				});
				alto = Math.max.apply(Math, altos);




				// console.log("window: " + wndW + " control: " + control_w );


				if (control_w >= screen_md) {
					$('#menu_completo > ul > li').height(alto);
				}

				if (control_w < screen_md) {
					$('#menu_completo > ul > li').height('auto');
				};

				if (control_w < screen_xs) {

				}

			}


			// ajuste alto twitter
			function calcularAlto2() {
				alto_agenda = $('#agenda').height();
				//  console.log('alto blogs: ' + alto_blogs + ' alto agenda: ' + alto_agenda);
				alto_twitter = alto_agenda;
				var wndW = $(window).width();

				if (control_w >= screen_sm) {
					$('#ultimos-tweets').height(alto_twitter);
				}
				if (control_w < screen_sm) {
					$('#ultimos-tweets').height('600px');
				};

			}

			// ---------------------------------------- smooth scroll  acordeon

			// sustituye los acrodeones de jquery por este que incorpora un scroll al punto que se despliega
			// son listas largas y si no se desplegaria fuera de pantalla
			// está metido en una función para poderla lanzar en eventos click además de otras funciones
			// como el plegado del bloque genérico de banners para mostrarlos encolumna derecha

			$('.acordion_smooth').click(function (event) {
				acordion_smooth(this, event);
			});

			$('.detalle_agenda').click(function (event) {
				acordion_smooth(this, event);
				$('#container_banners').slideUp('normal');
				// console.log('go');
			});

			$('.detalle_servicio').click(function (event) {
				acordion_smooth(this, event);
				$('#container_banners').slideUp('normal');
				// console.log('go');
			});

			function acordion_smooth(tl, event) {
				//  console.log(tl);
				target = $(tl.hash);
				th = $(tl);
				//  var target = tl.hash;
				//  console.log(target);
				estado = th.hasClass('activo');

				if (estado == false) {
					event.preventDefault();
					$(target).slideDown('normal');
					$('html, body').animate({
						scrollTop: target.offset().top
					}, 1000);
					th.toggleClass('activo');
				}
				if (estado == true) {
					event.preventDefault();

					$(target).slideUp('normal');
					th.toggleClass('activo');
				}
			}

			//----------------------listado resultados busador Colegiados

			li_result = $('.lista_result ul.dotted_list li a')
			ficha = $('.ficha_pro > div')

			li_result.hover(function () {
				mostrar_ficha(this);
			}, function () {
				//  th.toggleClass('activo');
			});



			function mostrar_ficha(tl) {
				target = $(tl.hash);
				th = $(tl);

				//  console.log(target);

				estado = th.hasClass('activo');
				if (estado == false) {
					event.preventDefault();
					$(li_result).removeClass('activo');
					$(ficha).hide(0, function () {
						$(target).show(0);
						th.toggleClass('activo');
					});
				}
				if (estado == true) {
					event.preventDefault();
				}
			}


			//---------------------leer más


			function leerMas(tl) {
				max_height = 190;
				leer_height = $(tl).height();
				texto_height = $(tl).find('.texto').height();

				if (texto_height > max_height) {
					$(tl).find('.crop').height(max_height);
					$(tl).find('.crop').addClass('ellipsis');
					$(tl).find('.crop').after('<a href="javascript:;" class="mas">leer más<span class="oculto">titulo entrada</span></a>')
				} else {}

				$(tl).on('click', '.mas', function (event) {
					texto_height = $(tl).find('.texto').height();
					// console.log ('click: ' + texto_height);
					$(tl).find('.crop').height(texto_height);
					$(tl).find('.crop').toggleClass('ellipsis');

					$(tl).find('.mas').text('leer menos');
					$(tl).find('.mas').toggleClass('menos');
					$(tl).find('.mas').toggleClass('mas');
				});

				$(tl).on('click', '.menos', function (event) {
					//  console.log ('click');
					$(tl).find('.crop').height(max_height);
					$(tl).find('.crop').toggleClass('ellipsis');
					$(tl).find('.menos').text('leer mas');
					$(tl).find('.menos').toggleClass('mas');
					$(tl).find('.menos').toggleClass('menos');
				});

			}

			$('.leermas').each(function () {
				leerMas(this);
			});


			//-------------------------------- back to top
			// no está de momento lanzado porque no son páginas muy largas.

			jQuery(window).scroll(function () {
				if (jQuery(this).scrollTop() > 2000) {
					jQuery('#back-to-top').fadeIn();
				} else {
					jQuery('#back-to-top').fadeOut();
				}
			});
			// scroll body to 0px on click
			jQuery('#back-to-top').click(function () {
				jQuery('#back-to-top').tooltip('hide');
				jQuery('body,html').animate({
					scrollTop: 0
				}, 800);
				return false;
			});

			jQuery('#back-to-top').tooltip();

		});
	}
);

Liferay.Portlet.ready(

	/*
	This function gets loaded after each and every portlet on the page.

	portletId: the current portlet's id
	node: the Alloy Node object of the current portlet
	*/

	function (portletId, node) {}
);

Liferay.on(
	'allPortletsReady',

	/*
	This function gets loaded when everything, including the portlets, is on
	the page.
	*/

	function () {

	}
);