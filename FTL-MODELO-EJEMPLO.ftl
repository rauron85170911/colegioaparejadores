<#assign liferay_ui = taglibLiferayHash["/WEB-INF/tld/liferay-ui.tld"] />
<#assign liferay_util = taglibLiferayHash["/WEB-INF/tld/liferay-util.tld"] />


<#assign aui = taglibLiferayHash["/WEB-INF/tld/aui.tld"] />
<!-- ThemeDisp<#assign liferay_ui = taglibLiferayHash["/WEB-INF/tld/liferay-ui.tld"] />
<#assign liferay_util = taglibLiferayHash["/WEB-INF/tld/liferay-util.tld"] />


<#assign aui = taglibLiferayHash["/WEB-INF/tld/aui.tld"] />
ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY); -->
<!-- comprobar login (boolean) -->
<#assign signedIn = themeDisplay.isSignedIn()>
<!--<#if signedIn>
    logadooo
<#else>
    no logado
</#if> -->
<div class="container-fluid">
    <#if entries?has_content>
    <section id="agenda">
        <h1>AGENDA</h1>
        <#if entries?has_content>  
            <#assign countR = 0>
            <ul>
                <#list entries as curEntry>        
        
                <#assign assetRenderer = curEntry.getAssetRenderer() />
                <#assign viewURL = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, curEntry) />
        
                <#if assetLinkBehavior != "showFullContent">
                    <#assign viewURL = assetRenderer.getURLViewInContext(renderRequest, renderResponse, viewURL) />
                </#if>
        
                <#assign document = saxReaderUtil.read(assetRenderer.getArticle().getContentByLocale(locale)) />
                <#assign date = document.valueOf("//dynamic-element[@name='Fecha']/dynamic-content/text()") />
        
                <#assign textocorto = document.valueOf("//dynamic-element[@name='Titulo_evento']/dynamic-content/text()") />
                <#assign tipoAgenda = document.valueOf("//dynamic-element[@name='Tipo_de_Agenda']/dynamic-content/text()") />
                <#assign DescEven = document.valueOf("//dynamic-element[@name='Descripcion_evento']/dynamic-content/text()") />
                <#assign FecEvento = document.valueOf("//dynamic-element[@name='Fecha_evento']/dynamic-content/text()") />
                <#assign fecDateObj = dateUtil.newDate(getterUtil.getLong(FecEvento))>
                    
                <#assign HoraDesde = document.valueOf("//dynamic-element[@name='Hora_desde']/dynamic-content/text()") />
                <#assign HoraHasta = document.valueOf("//dynamic-element[@name='Hora_hasta']/dynamic-content/text()") />
                

                 <#assign countR = countR + 1>
                

                <#switch tipoAgenda>
                    <#case "tipo1">
                        <li class="icon-lecturer-with-screen">
                        <#break>
                    <#case "tipo2">
                        <li class="icon-casa_enchufe">
                        <#break>
                    <#case "tipo3">
                        <li class="icon-birrete">
                        <#break>
                    <#case "tipo4">
                        <li class="icon-speech">
                        <#break>
                    <#case "tipo5">
                        <li class="icon-house-plan">
                        <#break>
                    <#case "tipo6">
                        <li class="icon-formacion-presencial">
                        <#break>
                    <#case "tipo7">
                        <#if signedIn>
                            <li class="colegiado icon-compas_aparejadores">
                        <#else>                
                            <li class="icon-compas_aparejadores">
                        </#if>                
                        <#break>
                    <#case "tipo8">
                        <li class="icon-aparejador">
                        <#break>
                </#switch>        
                
                    <div class="detalle">
                        <h3>
                        <a href="#contenido_detalle${countR}" class="detalle_agenda">${curEntry.getTitle(locale)}</a>
                        </h3>
                        <p>${dateUtil.getDate(fecDateObj, "dd/MM/yyyy", locale)}. ${HoraDesde} - ${HoraHasta}</p>
                        <p>${DescEven}</p>
                    </div>

                    
                </li>
                </#list>
            </ul>     
            
            <div class="botonera">
                <a class="boton_corp" href="/agenda/eventos-pasados" >
                <span class="light">ver</span>eventos<span class="light">pasados</span></a>
            </div>
        </#if>
    </section>
    </#if>
    
    
</div>

<#assign countR = 0>
<#if entries?has_content>
    <#if entries?has_content>  

        <#list entries as curEntry>    
            <#assign countR = countR + 1>
            <div class="container-100 collapse" id="contenido_detalle${countR}" >   
                <div class="container-fluid">    
            
                    <#assign assetRenderer = curEntry.getAssetRenderer() />
                    <#assign viewURL = assetPublisherHelper.getAssetViewURL(renderRequest, renderResponse, curEntry) />
                
                    <#if assetLinkBehavior != "showFullContent">
                        <#assign viewURL = assetRenderer.getURLViewInContext(renderRequest, renderResponse, viewURL) />
                    </#if>
                
                    <#assign document = saxReaderUtil.read(assetRenderer.getArticle().getContentByLocale(locale)) />
                    <#assign date = document.valueOf("//dynamic-element[@name='Fecha']/dynamic-content/text()") />
                
                    <#assign FecEvento = document.valueOf("//dynamic-element[@name='Fecha_evento']/dynamic-content/text()") />
                    <#assign fecDateObj = dateUtil.newDate(getterUtil.getLong(FecEvento))>
                    <#assign HoraDesde = document.valueOf("//dynamic-element[@name='Hora_desde']/dynamic-content/text()") />
                    <#assign HoraHasta = document.valueOf("//dynamic-element[@name='Hora_hasta']/dynamic-content/text()") />
                    <#assign ImagenDetalle = document.valueOf("//dynamic-element[@name='Imagen_Detalle']/dynamic-content/text()") />
                    <#assign DetalleAgenda = document.valueOf("//dynamic-element[@name='Detalle_Agenda']/dynamic-content/text()") />
                    <#assign Lugar = document.valueOf("//dynamic-element[@name='Lugar']/dynamic-content/text()") />
                    <#assign Online = document.valueOf("//dynamic-element[@name='Online']/dynamic-content/text()") />
                    <#assign Precio = document.valueOf("//dynamic-element[@name='Precio']/dynamic-content/text()") />                    
                    <#assign Inscripciones = document.valueOf("//dynamic-element[@name='Inscripciones']/dynamic-content/text()") />                    
                    <#assign countR = countR + 1>
                        
                    <section class="cont_generico">
                        <div class="main_column">
                            <h3>${curEntry.getTitle(locale)}</h3>
                            <#if ImagenDetalle?has_content>
                                <p><img alt="Ir al detalle de la noticia" src="${ImagenDetalle}" title="Imagen detalle" </p>
                            </#if>
                            ${DetalleAgenda}

                            <div class="ficha">
                                <h2>Información<span class="light">e</span>inscripciones</h2>
                                <ul>
                                    <#if fecDateObj?has_content>
                                        <li class="fecha"><p>del 25 de septiembre al 11 de diciembre 2018</p></li>
                                    </#if>
                                    <#if HoraDesde?has_content>
                                        <li class="hora"><p>${HoraDesde} 
                                        <#if HoraHasta?has_content>    
                                            a ${HoraHasta}
                                        </#if>                                        
                                        </p></li>
                                    </#if>
                                    <#if Lugar?has_content>
                                        <li class="lugar">${Lugar}</li>
                                    </#if>
                                    <#if Online?has_content>
                                        <li class="online">${Online}</li>
                                    </#if>
                                    <#if Precio?has_content>
                                        <li class="precio">${Precio}</p></li>
                                    </#if>
                                    <#if Inscripciones?has_content>
                                        <li class="inscripciones">${Inscripciones}</li>
                                    </#if>
                                </ul>    
                            </div>                            
                        </div>


                        <#assign Banners = document.valueOf("//dynamic-element[@name='Banners']/dynamic-content/text()") />                    
                        <#assign NumBanners = document.selectNodes("/root/dynamic-element[@name='Banners']")/>

                        
                        <#if Banners?has_content>
                            <aside class="aside_right">
                                <div id="banners">        
                                    <ul>
                                    <#list NumBanners as nodo>
                                    <#assign nodoURL = nodo.selectSingleNode("dynamic-element[@name='URL_Banners']/dynamic-content") />
                                    <#assign nodoIMG = nodo.selectSingleNode("dynamic-element[@name='Imagen_Banners']/dynamic-content") />
                                        <li><a href="${nodoURL.getText()}"
                                        target="_blank" title="Ir a la página ">
                                        <img  src="${nodoIMG.getText()}"
                                        alt="Imagen Banners"></a>
                                        </li>                        
                                    </#list>
                                    </ul>
                                </div>    
                            </aside>
                        </#if>        
                        
                    </section>
                </div>
            </div>    
        </#list>

    </#if>
</#if>