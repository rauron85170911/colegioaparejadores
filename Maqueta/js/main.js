// JavaScript Document

jQuery(document).ready(function() {


// menu desplegable cabeceras


jQuery(function () {
 var b_main = $(".navbar-toggle");
 var b_desp = $(".cab-nav .nav-item ");
 var desplegable = $('#navbar');

 function desplegar(tl) {
 	//		console.log('abre: ' + this);
 		 	tl.parent().children('#navbar').stop(true, true).delay(0).slideDown(200);
 		 	tl.toggleClass('open');
 	}

 	function plegar(tl) {
 	//	console.log('cierra: ' + tl);
 	 tl.parent().children('#navbar').stop(true, true).delay(0).slideUp(200);
 	 tl.toggleClass('open');
 	}

function desplegarSub(tl) {
			//console.log(this);
		  tl.siblings('.nav-item').children('.sub-menu').stop(true, true).delay(0).slideUp(200); //recoge otros dropdowns sin delay
		 	tl.children('.sub-menu').stop(true, true).delay(100).slideDown(200);
		 	tl.toggleClass('drop_over');
	}

	function plegarSub(tl) {
		//console.log(tl);
	 tl.children('.sub-menu').stop(true, true).delay(500).slideUp(200);
	 tl.toggleClass('drop_over');
	}


 var ancho_min = 630;
 var ancho_med = 930;
 var ancho_max = 1280;


 b_desp.hover( function() {
			desplegarSub(jQuery(this));
		}
		,function() {
			plegarSub(jQuery(this));
		}
 );


 b_main.click( function() {
     desplegar(jQuery(this));
   }
 );

 b_main.click( function() {
   	if (desplegable.is(':hidden') == true) {
       desplegar(jQuery(this));
    }
    else {
      plegar(jQuery(this));
    }
   }
 );


 $(document).on('click', function(event) {
   if (!$(event.target).closest('.navbar').length) {
     // stopping-event-propagation
    // console.log('cierra');
     plegar($('#navbar'));
   }
 });

 });




 /*-----------------------resize servicios home------------------------*/

// hack para equiparar los anchos del @media query con los de jquery
$('body').append("<div id='aux'><div></div></div>");

 var wndW ;
 var control_w;

 var screen_xxs = 390;
 var screen_xs = 480;
 var screen_s = 570;
 var screen_is = 620;
 var screen_sm = 768;
 var screen_md = 992;
 var screen_lg = 1260;

function width_trace() {
       wndW = $(window).width();
       control_w = $("body").find('#aux div').width();
      // console.log("window width: " + wndW + " , control_w: " + control_w);
}

 width_trace();
 calcularAlto();
 calcularAlto2();
 calcularalto3();

$( window ).resize(function() {
       width_trace();
       calcularAlto();
       calcularAlto2();
        calcularalto3();
  //     console.log ("resize");
 });



 $( window ).on( "orientationchange", function( event ) {
  // console.log( "Este dispositivo está en modo " + event.orientation);
   width_trace();
   calcularAlto();
   calcularAlto2();
    calcularalto3();
 });

// ajuste de alto de los parrafos de mundo servicios
function calcularAlto(){
 alto_1 = $('.auto_height_1 p').outerHeight();
 alto_2 = $('.auto_height_2 p').outerHeight();
 alto_3 = $('.auto_height_3 p').outerHeight();


 // console.log(alto_1 + " , " + alto_2 + " , " + alto_3 + " ,el más alto: " + mas_alto);
  // console.log("window: " + wndW + " control: " + control_w );


  if (control_w >= screen_sm){
     mas_alto = Math.max(alto_1, alto_2, alto_3);
    $('.auto_height_1').height(mas_alto);
    $('.auto_height_2').height(mas_alto);
    $('.auto_height_3').height(mas_alto);
  }
  if((control_w < screen_sm) & (control_w >= screen_xs)) {
    //		console.log("menos");
    mas_alto = Math.max(alto_1, alto_2);
    $('.auto_height_1').height(mas_alto);
    $('.auto_height_2').height(mas_alto);
    $('.auto_height_3').height('auto');
  };
  if(control_w < screen_xs){
    $('.auto_height_1').height('auto');
    $('.auto_height_2').height('auto');
    $('.auto_height_3').height('auto');
  }

}


// ajuste de alto de los bloques menú
function  calcularalto3(){

  var altos = [];
 $('#menu_completo > ul > li').each(function() {
        altos.push($(this).height());
    //   console.log($(this).height());
 });
     alto = Math.max.apply(Math, altos);




  // console.log("window: " + wndW + " control: " + control_w );


  if (control_w >= screen_md){
       $('#menu_completo > ul > li').height(alto);
  }

  if(control_w < screen_md) {
      $('#menu_completo > ul > li').height('auto');
  };

  if(control_w < screen_xs){

  }

}


// ajuste alto twitter
function calcularAlto2(){
alto_agenda = $('#agenda').height();
//  console.log('alto blogs: ' + alto_blogs + ' alto agenda: ' + alto_agenda);
alto_twitter = alto_agenda ;
var wndW = $(window).width();

 if (control_w  >= screen_sm){
     $('#ultimos-tweets').height(alto_twitter);
 }
 if(control_w  < screen_sm) {
     $('#ultimos-tweets').height('600px');
 };

}

// ---------------------------------------- smooth scroll  acordeon

// sustituye los acrodeones de jquery por este que incorpora un scroll al punto que se despliega
// son listas largas y si no se desplegaria fuera de pantalla
// está metido en una función para poderla lanzar en eventos click además de otras funciones
// como el plegado del bloque genérico de banners para mostrarlos encolumna derecha

$('.acordion_smooth').click(function(event) {
  acordion_smooth(this,event);
});

$('.detalle_agenda').click(function(event) {
  acordion_smooth(this,event);
  $('#container_banners').slideUp('normal');
  // console.log('go');
});

$('.detalle_servicio').click(function(event) {
  acordion_smooth(this,event);
  $('#container_banners').slideUp('normal');
  // console.log('go');
});

function acordion_smooth(tl,event){
//  console.log(tl);
 target = $(tl.hash);
 th = $(tl);
//  var target = tl.hash;
//  console.log(target);
 estado = th.hasClass('activo');

 if(estado == false) {
   event.preventDefault();
  $(target).slideDown('normal');
   $('html, body').animate({
       scrollTop: target.offset().top
   }, 1000);
     th.toggleClass('activo');
 }
 if(estado == true) {
   event.preventDefault();

   $(target).slideUp('normal');
    th.toggleClass('activo');
 }
}

//----------------------listado resultados busador Colegiados

    li_result = $('.lista_result ul.dotted_list li a')
    ficha = $('.ficha_pro > div')

    li_result.hover( function() {
     mostrar_ficha(this);
      }
   ,function() {
  //  th.toggleClass('activo');
   }
);



function mostrar_ficha(tl){
  target = $(tl.hash);
  th = $(tl);

//  console.log(target);

  estado = th.hasClass('activo');
  if(estado == false) {
    event.preventDefault();
    $(li_result).removeClass('activo');
    $(ficha).hide( 0, function() {
         $(target).show(0);
         th.toggleClass('activo');
      });
  }
  if(estado == true) {
     event.preventDefault();
  }
}


//---------------------leer más


function leerMas(tl){
   max_height = 190;
   leer_height = $(tl).height();
   texto_height = $(tl).find('.texto').height();

   if ( texto_height >  max_height  ) {
     $(tl).find('.crop').height(max_height);
     $(tl).find('.crop').addClass('ellipsis');
     $(tl).find('.crop').after('<a href="javascript:;" class="mas">leer más<span class="oculto">titulo entrada</span></a>')
   }
   else {
   }

   $(tl).on('click','.mas', function(event) {
     texto_height = $(tl).find('.texto').height();
      // console.log ('click: ' + texto_height);
      $(tl).find('.crop').height(texto_height);
      $(tl).find('.crop').toggleClass('ellipsis');

      $(tl).find('.mas').text('leer menos');
      $(tl).find('.mas').toggleClass('menos');
      $(tl).find('.mas').toggleClass('mas');
   });

   $(tl).on('click','.menos', function(event) {
    //  console.log ('click');
      $(tl).find('.crop').height(max_height);
      $(tl).find('.crop').toggleClass('ellipsis');
      $(tl).find('.menos').text('leer mas');
      $(tl).find('.menos').toggleClass('mas');
      $(tl).find('.menos').toggleClass('menos');
   });

}

$('.leermas').each(function() {
      leerMas(this);
});


//-------------------------------- back to top
// no está de momento lanzado porque no son páginas muy largas.

 jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 2000) {
                jQuery('#back-to-top').fadeIn();
            } else {
                jQuery('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        jQuery('#back-to-top').click(function () {
            jQuery('#back-to-top').tooltip('hide');
            jQuery('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

				jQuery('#back-to-top').tooltip();

});
